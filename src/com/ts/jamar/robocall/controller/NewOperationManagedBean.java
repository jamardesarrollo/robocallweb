package com.ts.jamar.robocall.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;

import com.jamar.login.security.controller.LoginManagedBean;
import com.ts.jamar.entities.RbcCampana;
import com.ts.jamar.exceptions.RobocallException;
import com.ts.jamar.services.CampanaServicesLocal;
import com.ts.jamar.util.CampanaEstadoEnum;

@SessionScoped
@ManagedBean
public class NewOperationManagedBean {

	@EJB
	private CampanaServicesLocal campanaServicesLocal;
	
	@ManagedProperty(value="#{loginManagedBean}")
    private LoginManagedBean loginManagedBean;
	@ManagedProperty(value="#{principalManagedBean}")
	private PrincipalManagedBean principalManagedBean;
	private UploadedFile voiceFile;
	@Deprecated
	private File clientFile;
	private UploadedFile otherFile;
	private RbcCampana current_item;
	private boolean edit;
	private CampanaEstadoEnum currentStatus;
	private String destinationAbsoluteFile;
	private String destinationAbsolute;
	
	
	
	@PostConstruct
	private void initComponent(){
		try {
			this.otherFile=null;
			this.voiceFile=null;
			this.edit=true;
			this.currentStatus=CampanaEstadoEnum.CREATE;
			this.principalManagedBean.setMenuTabIndex(1);
		} catch (Exception e) {
			e.printStackTrace();
			this.showMessage(FacesMessage.SEVERITY_ERROR,"Mensaje de Informaci�n", "Ha ocurrido un error, "
					+ "contacte al administrador");
		}
	}
	
	/**
	 * M�todo utilizado para inicializar el formulario
	 * de la vista desde otro bean
	 * @param campana
	 */
	public void initCurrentItem(RbcCampana campana){
		this.edit=true;
		this.currentStatus=CampanaEstadoEnum.CREATE;
		this.current_item=campana;

		
		if(this.current_item==null){
			this.current_item=new RbcCampana();
			this.edit=false;
		}
		System.out.println("this.current_item.getRbcEstados(): "+this.current_item.getRbcEstados());
		if(this.current_item.getRbcEstados()!=null){
			
			if(this.current_item.getRbcEstados().getRbcEstadosPK().getCons().intValue()==CampanaEstadoEnum.INITIATED.getCode())
				this.currentStatus=CampanaEstadoEnum.INITIATED;
			
			else if(this.current_item.getRbcEstados().getRbcEstadosPK().getCons().intValue()==CampanaEstadoEnum.FINALIZED.getCode())
				this.currentStatus=CampanaEstadoEnum.FINALIZED;
			
		}
		System.out.println("this.currentStatus => "+this.currentStatus);
		
	}
	
	/**
	 * Metodo implemntado para mostrar mensajes
	 * de advertencia error y/o informativos
	 * apartir de una accion ejecutada.
	 * 
	 * @param messageType
	 * @param tittle
	 * @param body
	 */
	private void showMessage(Severity messageType,String tittle, String body) {
        FacesMessage message = new FacesMessage( messageType, tittle, body);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }

	
	/**
	 * M�too que se utiliza para guardar la informaci�n de 
	 * la campana crea o edita el item de la campa�a
	 */
	public void save(){
		try {
            /**
             * Metodo implementado para asignar la
             * informaci�n de los telefonos ingresados.
             */
         
            
            
            if(this.current_item.getNombre()==null || this.current_item.getNombre().trim().isEmpty())
            	throw new IllegalArgumentException("Debe ingresar un nombre para la campa�a");
            
           
			if(!this.edit){
				this.campanaServicesLocal
							.saveCampana(this.loginManagedBean.getMarket(),
									     this.loginManagedBean.getUserName(),
									     this.current_item,
									     (this.voiceFile!=null ? this.voiceFile.getInputstream(): null),
									     (this.otherFile!=null ? this.otherFile.getInputstream(): null),
									     (this.voiceFile!=null ? this.voiceFile.getFileName(): null),
									     (this.otherFile!=null ? this.otherFile.getFileName(): null)
									     );
				
				this.showMessage(FacesMessage.SEVERITY_INFO,"Mensaje de Informaci�n", "La campa�a a sido creada exitosamente.");
				
				this.returnParametri();
			}else{
				
				
				this.campanaServicesLocal
							.editCampana(this.loginManagedBean.getMarket(),
									     this.loginManagedBean.getUserName(),
									     this.current_item,
									     (this.voiceFile!=null ? this.voiceFile.getInputstream(): null),
									     (this.otherFile!=null ? this.otherFile.getInputstream(): null),
									     (this.voiceFile!=null ? this.voiceFile.getFileName(): null),
									     (this.otherFile!=null ? this.otherFile.getFileName(): null));
				
				this.showMessage(FacesMessage.SEVERITY_INFO,"Mensaje de Informaci�n", "La campa�a a sido editada exitosamente.");
				this.returnParametri();
			}
			
			
					
			
		}catch(IllegalArgumentException e){
			e.printStackTrace();
			System.out.println("Etro por illegalargument >>>>>");
			this.showMessage(FacesMessage.SEVERITY_WARN,"Mensaje de Informaci�n", e.getMessage());
			
		} catch (RobocallException e) {
			e.printStackTrace();
			this.showMessage(FacesMessage.SEVERITY_WARN,"Mensaje de Informaci�n", e.getMessage());
		} catch(Exception e){
			e.printStackTrace();
			this.showMessage(FacesMessage.SEVERITY_ERROR,"Mensaje de Informaci�n", "Ha ocurrido un error, "
					+ "contacte al administrador");
		}
	}

	private void returnParametri() throws IOException {
		FacesContext ctx=FacesContext.getCurrentInstance();
		ExternalContext context= ctx.getExternalContext();
		context.redirect("/RobocallWeb/parametrization.xhtml");
	}
	
	/**
	 * Metodo implementado para cargar
	 * audio
	 */
	@Deprecated
	public void uploadMedia(){
		try {
			System.out.println("Entro a metodo upload Media");
			System.out.println(this.voiceFile.getFileName());
			ServletContext ctx= (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String path=ctx.getRealPath("/");
			this.destinationAbsolute=path+"resources/audio/";

    		this.createFile(this.voiceFile.getFileName(),this.voiceFile.getInputstream(),this.destinationAbsolute);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Metodo implmentado para cargar archivo
	 * 
	 * @param event
	 */
	@Deprecated
	public void uploadFile() {
		try{
			
//			UploadedFile file=event.getFile();
			System.out.println("Entro a metodo de carga de archivo....");
			
			ServletContext ctx= (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String path=ctx.getRealPath("/");
			this.destinationAbsoluteFile=path+"resources/files/";
			
			System.out.println("Llamando metodo createFile");
	        		this.createFile(this.otherFile.getFileName(),this.otherFile.getInputstream(),this.destinationAbsoluteFile);
	        
		}catch(Exception e){
			e.printStackTrace();
		}   
    }
	
	/**
	 * Metodo implementado para crear 
	 * el archivo seleccionado.
	 * 
	 * @param reader
	 * @param fileName
	 * @return
	 */
	private void createFile(String fileName, InputStream fileInputStream, String destination){
		try{
			System.out.println("Entro a metodo create file ...");
			OutputStream fileOutputStram=new FileOutputStream(new File(destination+fileName));
			
			int read =0;
			byte[] bytes=new byte[1024];
			
			while ((read=fileInputStream.read(bytes))!=-1) {
				fileOutputStram.write(bytes, 0, read);
			}
			
			fileInputStream.close();
			fileOutputStram.flush();
			fileOutputStram.close();
			
			
		}catch(IOException ex){
			ex.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo implementado para iniciar
	 * una campa�a creada en el sistem.
	 */
	public void start(){
		try{
			this.campanaServicesLocal.startCampana(this.loginManagedBean.getMarket(),
									     this.loginManagedBean.getUserName(),
									     this.current_item);
			
			RbcCampana item=this.campanaServicesLocal
					.findCampana(this.loginManagedBean.getMarket(), this.current_item.getRbcCampanaPK().getCons());
			
			this.initCurrentItem(item);
			
			this.showMessage(FacesMessage.SEVERITY_INFO,"Mensaje de Informaci�n", 
								"La campa�a a sido iniciada exitosamente.");
			
		} catch (RobocallException e){
			this.showMessage(FacesMessage.SEVERITY_WARN,"Mensaje de Informaci�n",e.getMessage());
			
		} catch (Exception e){
			e.printStackTrace();
			this.showMessage(FacesMessage.SEVERITY_ERROR,"Mensaje de Informaci�n", 
								"Ha ocurrido un error iniciando la campa�a, contacte al administrador.");
		}
	}
	
	
	public LoginManagedBean getLoginManagedBean() {
		return loginManagedBean;
	}

	public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
		this.loginManagedBean = loginManagedBean;
	}

	/**
	 * @return the voiceFile
	 */
	public UploadedFile getVoiceFile() {
		return voiceFile;
	}

	/**
	 * @return the clientFile
	 */
	public File getClientFile() {
		return clientFile;
	}

	/**
	 * @param voiceFile the voiceFile to set
	 */
	public void setVoiceFile(UploadedFile voiceFile) {
		this.voiceFile = voiceFile;
	}

	/**
	 * @param clientFile the clientFile to set
	 */
	public void setClientFile(File clientFile) {
		this.clientFile = clientFile;
	}

	/**
	 * @return the current_item
	 */
	public RbcCampana getCurrent_item() {
		return current_item;
	}

	/**
	 * @param current_item the current_item to set
	 */
	public void setCurrent_item(RbcCampana current_item) {
		this.current_item = current_item;
	}

	/**
	 * @return the edit
	 */
	public boolean isEdit() {
		return edit;
	}

	/**
	 * @param edit the edit to set
	 */
	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	/**
	 * @return the currentStatus
	 */
	public CampanaEstadoEnum getCurrentStatus() {
		return currentStatus;
	}

	/**
	 * @param currentStatus the currentStatus to set
	 */
	public void setCurrentStatus(CampanaEstadoEnum currentStatus) {
		this.currentStatus = currentStatus;
	}

	/**
	 * @return the otherFile
	 */
	public UploadedFile getOtherFile() {
		return otherFile;
	}

	/**
	 * @param otherFile the otherFile to set
	 */
	public void setOtherFile(UploadedFile otherFile) {
		this.otherFile = otherFile;
	}

	/**
	 * @return the principalManagedBean
	 */
	public PrincipalManagedBean getPrincipalManagedBean() {
		return principalManagedBean;
	}

	/**
	 * @param principalManagedBean the principalManagedBean to set
	 */
	public void setPrincipalManagedBean(PrincipalManagedBean principalManagedBean) {
		this.principalManagedBean = principalManagedBean;
	}

	@Deprecated
	public void upload() {
		
        if(this.otherFile != null) {
            FacesMessage message = new FacesMessage("Succesful", this.otherFile.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
	

}
