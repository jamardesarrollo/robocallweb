package com.ts.jamar.robocall.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import com.jamar.login.security.controller.LoginManagedBean;
import com.ts.jamar.entities.RbcCampana;
import com.ts.jamar.services.CampanaServicesLocal;

@ViewScoped
@ManagedBean
public class ParametrizationManagedBean {

	@EJB
	private CampanaServicesLocal campanaServicesLocal;
	
	
	private List<RbcCampana> campanas;
	
	
	
    @ManagedProperty(value="#{loginManagedBean}")
    private LoginManagedBean loginManagedBean;
    
	@ManagedProperty(value="#{principalManagedBean}")
	private PrincipalManagedBean principalManagedBean;
	
	@ManagedProperty(value="#{newOperationManagedBean}")
	private NewOperationManagedBean newOperationManagedBean;
	
	@PostConstruct
	private void initComponent(){
		this.campanas=new ArrayList<RbcCampana>();
		this.loadCampanas();
		this.principalManagedBean.setMenuTabIndex(1);
	}
	
	/**
	 * M�todo utilizad para cargar la informaci�n
	 * del formulario de creaci�n o edici�n de la campa�a.
	 * 
	 * @param event
	 */
	public void loadView(ActionEvent event){
		RbcCampana rbc= (RbcCampana) event.getComponent().getAttributes().get("item");
		
		if(this.newOperationManagedBean!=null)
			this.newOperationManagedBean.initCurrentItem(rbc);
	}	
	
	public void loadNewForm(){
		if(this.newOperationManagedBean!=null)
			this.newOperationManagedBean.initCurrentItem(null);
	}
	/**
	 * M�todo utilizado para cargar la tabla de las 
	 * campa�as
	 * */
	private void loadCampanas(){
		try{
			this.campanas=this.campanaServicesLocal.findCampanas(this.loginManagedBean.getMarket());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * @return the campanas
	 */
	public List<RbcCampana> getCampanas() {
		return campanas;
	}

	/**
	 * @param campanas the campanas to set
	 */
	public void setCampanas(List<RbcCampana> campanas) {
		this.campanas = campanas;
	}

	/**
	 * @return the principalManagedBean
	 */
	public PrincipalManagedBean getPrincipalManagedBean() {
		return principalManagedBean;
	}

	/**
	 * @param principalManagedBean the principalManagedBean to set
	 */
	public void setPrincipalManagedBean(PrincipalManagedBean principalManagedBean) {
		this.principalManagedBean = principalManagedBean;
	}
	
	public LoginManagedBean getLoginManagedBean() {
		return loginManagedBean;
	}

	public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
		this.loginManagedBean = loginManagedBean;
	}
	
	
	/**
	 * @return the newOperationManagedBean
	 */
	public NewOperationManagedBean getNewOperationManagedBean() {
		return newOperationManagedBean;
	}

	/**
	 * @param newOperationManagedBean the newOperationManagedBean to set
	 */
	public void setNewOperationManagedBean(NewOperationManagedBean newOperationManagedBean) {
		this.newOperationManagedBean = newOperationManagedBean;
	}

	
	
	
}
