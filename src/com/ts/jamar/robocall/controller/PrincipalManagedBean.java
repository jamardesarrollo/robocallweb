package com.ts.jamar.robocall.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class PrincipalManagedBean {
	
	private Integer menuTabIndex=0;

	/**
	 * @return the menuTabIndex
	 */
	public Integer getMenuTabIndex() {
		return menuTabIndex;
	}

	/**
	 * @param menuTabIndex the menuTabIndex to set
	 */
	public void setMenuTabIndex(Integer menuTabIndex) {
		this.menuTabIndex = menuTabIndex;
	}
	
	

}
