package com.ts.jamar.robocall.controller;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import org.primefaces.model.chart.PieChartModel;

import com.jamar.login.security.controller.LoginManagedBean;
import com.ts.jamar.entities.RbcCampana;
import com.ts.jamar.exceptions.RobocallException;
import com.ts.jamar.exceptions.RobocallExceptionEnum;
import com.ts.jamar.services.CampanaServicesLocal;

@SessionScoped
@ManagedBean
public class DashboardManagedBean {

	@EJB
	private CampanaServicesLocal campanaServicesLocal;
	
	@ManagedProperty(value="#{loginManagedBean}")
    private LoginManagedBean loginManagedBean;
	
	@ManagedProperty(value="#{principalManagedBean}")
	private PrincipalManagedBean principalManagedBean;
	
	private List attempts;
	private Date initialDate;
	private Date finalDate;
	private BigInteger consCampana;
	
	
	private PieChartModel customAnswer;
	private PieChartModel sourceAnswer;
	private List<SelectItem> campanaItems;
	
	
	@PostConstruct
	private void postContruct(){
		this.initialDate=new Date();
		this.finalDate=new Date();
		this.consCampana=BigInteger.valueOf(-1);
		this.attempts=new ArrayList();
		this.campanaItems=new ArrayList<SelectItem>();
		
		this.loadAttempts();
		this.loadCampanas();
		this.createCustomAnswer();
		this.createSourceAnswer();
		this.principalManagedBean.setMenuTabIndex(0);
	
	}
	
	
	/**
	 * M�todo que carga las campa�as
	 */
	private void loadCampanas(){
		try {
			if(this.campanaItems!=null)this.campanaItems.clear();
			this.campanaItems.add(new SelectItem(-1,"--Seleccione la campa�a----") );
			List<RbcCampana> cmps=this.campanaServicesLocal.findCampanas(this.loginManagedBean.getMarket());
			for (int i = 0; i < cmps.size(); i++) 
				this.campanaItems
						.add(new SelectItem(cmps.get(i).getRbcCampanaPK().getCons(),cmps.get(i).getNombre()));
			
			
		} catch (RobocallException e) {
			if(!e.getErrorEnum().equals(RobocallExceptionEnum.NO_RESULT))
				System.out.println("aqui lanzo una exception");
		}
	}
	/**
	 * M�todo utilizado para cargas los
	 * ultimos intentos de llamadas
	 */
	private void loadAttempts(){
		try {
			System.out.println("fecha inicio => "+this.initialDate);
			System.out.println("fecha fin => "+this.finalDate);
			this.attempts=this.campanaServicesLocal
					.lastAttempts(this.loginManagedBean.getMarket(),this.consCampana,this.initialDate,this.finalDate);
			
			
		} catch (RobocallException e) {
			if(!e.getErrorEnum().equals(RobocallExceptionEnum.NO_RESULT))
				System.out.println("aqui lanzo una exception");
		}
	}
	
	private void createCustomAnswer() {
		try{
			this.customAnswer = new PieChartModel();
			List<Object[]> list=this.campanaServicesLocal
					.findCustomAnswer(this.loginManagedBean.getMarket(), 
									  this.consCampana, 
									  this.initialDate, this.finalDate);
			for (int i = 0; i < list.size(); i++) {
				this.customAnswer.set("Clientes exitosos", (BigDecimal)list.get(i)[1]);
				this.customAnswer.set("Clientes medio", (BigDecimal)list.get(i)[2]);
			}
			
			this.customAnswer.setTitle("Respuestas de los clientes");
			this.customAnswer.setLegendPosition("e");
			this.customAnswer.setFill(true);
			this.customAnswer.setShowDataLabels(true);
			this.customAnswer.setDiameter(150);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}  
    }
	
	/**
	 * M�todo utilizado para recargas los componentes
	 * de la pagina
	 */
	public void updatePage() {
        try{
        	
        	this.loadAttempts();
    		this.loadCampanas();
    		this.createCustomAnswer();
    		this.createSourceAnswer();
    		
        }catch(Exception ex){
        	ex.printStackTrace();
        }
    }
	
	
	private void createSourceAnswer() {
		try{
			this.sourceAnswer = new PieChartModel();
			List<Object[]> list=this.campanaServicesLocal
					.findSourceAnswer(this.loginManagedBean.getMarket(), 
									  this.consCampana, 
									  this.initialDate, this.finalDate);
			for (int i = 0; i < list.size(); i++) {
				this.sourceAnswer.set("Llamada", (BigDecimal)list.get(i)[1]);
				this.sourceAnswer.set("Correo electronico", (BigDecimal)list.get(i)[2]);
			}
			
			this.sourceAnswer.setTitle("Origen de las respuestas");
			this.sourceAnswer.setLegendPosition("e");
			this.sourceAnswer.setFill(true);
			this.sourceAnswer.setShowDataLabels(true);
			this.sourceAnswer.setDiameter(150);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}  
    }
	/**
	 * @return the loginManagedBean
	 */
	public LoginManagedBean getLoginManagedBean() {
		return loginManagedBean;
	}

	/**
	 * @param loginManagedBean the loginManagedBean to set
	 */
	public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
		this.loginManagedBean = loginManagedBean;
	}
	/**
	 * @return the attempts
	 */
	public List getAttempts() {
		return attempts;
	}
	/**
	 * @param attempts the attempts to set
	 */
	public void setAttempts(List attempts) {
		this.attempts = attempts;
	}

	/**
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * @return the consCampana
	 */
	public BigInteger getConsCampana() {
		return consCampana;
	}

	/**
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * @param consCampana the consCampana to set
	 */
	public void setConsCampana(BigInteger consCampana) {
		this.consCampana = consCampana;
	}
	/**
	 * @return the campanaItems
	 */
	public List<SelectItem> getCampanaItems() {
		return campanaItems;
	}
	/**
	 * @param campanaItems the campanaItems to set
	 */
	public void setCampanaItems(List<SelectItem> campanaItems) {
		this.campanaItems = campanaItems;
	}
	/**
	 * @return the customAnswer
	 */
	public PieChartModel getCustomAnswer() {
		return customAnswer;
	}
	/**
	 * @return the sourceAnswer
	 */
	public PieChartModel getSourceAnswer() {
		return sourceAnswer;
	}
	/**
	 * @param customAnswer the customAnswer to set
	 */
	public void setCustomAnswer(PieChartModel customAnswer) {
		this.customAnswer = customAnswer;
	}
	/**
	 * @param sourceAnswer the sourceAnswer to set
	 */
	public void setSourceAnswer(PieChartModel sourceAnswer) {
		this.sourceAnswer = sourceAnswer;
	}

	/**
	 * @return the principalManagedBean
	 */
	public PrincipalManagedBean getPrincipalManagedBean() {
		return principalManagedBean;
	}
	/**
	 * @param principalManagedBean the principalManagedBean to set
	 */
	public void setPrincipalManagedBean(PrincipalManagedBean principalManagedBean) {
		this.principalManagedBean = principalManagedBean;
	}
		
}
