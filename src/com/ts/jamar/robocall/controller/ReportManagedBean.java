package com.ts.jamar.robocall.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.jamar.login.security.controller.LoginManagedBean;
import com.ts.jamar.entities.RbcCampana;
import com.ts.jamar.exceptions.RobocallException;
import com.ts.jamar.exceptions.RobocallExceptionEnum;
import com.ts.jamar.services.CampanaServicesLocal;

@ViewScoped
@ManagedBean
public class ReportManagedBean {

	@EJB
	private CampanaServicesLocal campanaServicesLocal;
	
	@ManagedProperty(value="#{loginManagedBean}")
    private LoginManagedBean loginManagedBean;
	@ManagedProperty(value="#{principalManagedBean}")
	private PrincipalManagedBean principalManagedBean;
	
	private List<SelectItem> campanaItems;
	
	private Date initialDate;
	private Date finalDate;
	private BigInteger consCampana;
	private Integer reportId;
	
	private StreamedContent file;
	
	
	@PostConstruct
	private void postConstruct(){
		this.campanaItems=new ArrayList<SelectItem>();
		this.consCampana=BigInteger.valueOf(-1);
		this.reportId=-1;
		this.initialDate=new Date();
		this.finalDate=new Date();
		this.loadCampanas();
		this.principalManagedBean.setMenuTabIndex(2);
	}
	
	private void showMessage(Severity messageType,String tittle, String body) {
        FacesMessage message = new FacesMessage( messageType, tittle, body);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
	
	public void generateExcel(ActionEvent event){
		try {
			File excel=null ;
			
			if(this.reportId==null)this.reportId=-1;
			
			switch (this.reportId) {
			
			case 1:  
				this.showMessage(FacesMessage.SEVERITY_INFO,"Mensaje de informaci�n", "No hay datos");
				return;
			
			case 2:  
            	excel = this.campanaServicesLocal
				.generateExcelCustomAnswer(this.loginManagedBean.getMarket(), this.consCampana, this.initialDate, this.finalDate);
               
            	break;
	          
			case 3:  
            	excel = this.campanaServicesLocal
				.generateExcelSourceAnswer(this.loginManagedBean.getMarket(), this.consCampana, this.initialDate, this.finalDate);
               
            	break;            	
	            	
	        case 4:  
	        	excel = this.campanaServicesLocal
				.generateExcelAttempts(this.loginManagedBean.getMarket(), this.consCampana, this.initialDate, this.finalDate);
	           
	        	break;
	            	
	            
	            default:
	            	this.showMessage(FacesMessage.SEVERITY_INFO,"Mensaje de informaci�n", "No hay datos");
					return;
			}
			
			
			 this.file = new DefaultStreamedContent(new FileInputStream(excel),
					 "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excel.getName());
			 System.out.println("this.file.getName() => "+this.file.getName());
			 
		} catch (RobocallException e) {
			e.printStackTrace();
			this.showMessage(FacesMessage.SEVERITY_INFO,"Mensaje de Informaci�n", e.getMessage());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			this.showMessage(FacesMessage.SEVERITY_ERROR,"Mensaje de Informaci�n", "Ha ocurrido un error generando el archivo, "
					+ "contacte al administrador");
		}
	}


	/**
	 * M�todo que carga las campa�as 
	 * creadas en el sistema.
	 */
	private void loadCampanas(){
		try {
			this.campanaItems.add(new SelectItem(-1,"--Seleccione la campa�a----") );
			List<RbcCampana> cmps=this.campanaServicesLocal.findCampanas(this.loginManagedBean.getMarket());
			for (int i = 0; i < cmps.size(); i++) 
				this.campanaItems
						.add(new SelectItem(cmps.get(i).getRbcCampanaPK().getCons(),cmps.get(i).getNombre()));
			
			
		} catch (RobocallException e) {
			if(!e.getErrorEnum().equals(RobocallExceptionEnum.NO_RESULT))
				System.out.println("aqui lanzo una exception");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @return the campanaItems
	 */
	public List<SelectItem> getCampanaItems() {
		return campanaItems;
	}
	/**
	 * @param campanaItems the campanaItems to set
	 */
	public void setCampanaItems(List<SelectItem> campanaItems) {
		this.campanaItems = campanaItems;
	}
	/**
	 * @return the loginManagedBean
	 */
	public LoginManagedBean getLoginManagedBean() {
		return loginManagedBean;
	}
	/**
	 * @param loginManagedBean the loginManagedBean to set
	 */
	public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
		this.loginManagedBean = loginManagedBean;
	}
	/**
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	/**
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	/**
	 * @return the consCampana
	 */
	public BigInteger getConsCampana() {
		return consCampana;
	}
	/**
	 * @return the reportId
	 */
	public Integer getReportId() {
		return reportId;
	}
	/**
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	/**
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	/**
	 * @param consCampana the consCampana to set
	 */
	public void setConsCampana(BigInteger consCampana) {
		this.consCampana = consCampana;
	}
	/**
	 * @param reportId the reportId to set
	 */
	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}
	/**
	 * @return the file
	 */
	public StreamedContent getFile() {
		return file;
	}
	/**
	 * @param file the file to set
	 */
	public void setFile(StreamedContent file) {
		this.file = file;
	}

	/**
	 * @return the principalManagedBean
	 */
	public PrincipalManagedBean getPrincipalManagedBean() {
		return principalManagedBean;
	}

	/**
	 * @param principalManagedBean the principalManagedBean to set
	 */
	public void setPrincipalManagedBean(PrincipalManagedBean principalManagedBean) {
		this.principalManagedBean = principalManagedBean;
	}
	
}
