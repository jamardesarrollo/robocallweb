package com.ts.jamar.supervision.utils;

import java.util.Iterator;

import javax.faces.component.UIComponent;

public class ComponentUtils {
	public static UIComponent findComponent(UIComponent component, String id) {
		if (id.equals(component.getId())) {
			return component;
		}

		Iterator<UIComponent> kids = component.getFacetsAndChildren();
		while (kids.hasNext()) {
			UIComponent kid = kids.next();
			UIComponent found = findComponent(kid, id);
			if (found != null) {
				return found;
			}
		}

		return null;
	}
	
	public static String month2String(int month){
	    String[] monthNames = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
	    return monthNames[month];
	}
}
