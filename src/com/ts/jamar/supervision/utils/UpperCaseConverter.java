package com.ts.jamar.supervision.utils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "UpperCaseConverter")
public class UpperCaseConverter implements Converter {

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		return value.toUpperCase().trim();
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object value) {
		return value.toString().trim();
	}

}
