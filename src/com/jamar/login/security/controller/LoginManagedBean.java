package com.jamar.login.security.controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.annotation.PostConstruct;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.jamar.beans.commons.AffiliateInformation;
import com.jamar.beans.security.LoginBean;
import com.jamar.beans.security.ResponseLoginBean;
import com.jamar.errorhandler.facade.IErrorHandlerFacade;
import com.jamar.errorhandler.facade.impl.ErrorHandlerFacade;
import com.jamar.exception.AdapterException;
import com.jamar.exception.BusinessException;
import com.jamar.exception.DAOException;
import com.jamar.exception.DataException;
import com.jamar.exception.ExternalSystemNotAvailable;
import com.jamar.exception.PropertyException;
import com.jamar.login.security.delegate.BusinessDelegate;
import com.jamar.properties.constant.Constant;
import com.jamar.properties.facade.IPropertiesFacade;
import com.jamar.properties.facade.impl.PropertiesFacade;

@SessionScoped
public class LoginManagedBean {

	private BusinessDelegate businessDelegate;
	private IPropertiesFacade propertiesFacade;
	private IErrorHandlerFacade errorHandlerFacade;
	private String userName;
	private String password;
	private String affiliateId;
	private String market;
	private String businessDomain;
	private String system;
	private String theme;
	public static final Logger logger = LogManager.getLogger(LoginManagedBean.class);
	
	
	@PostConstruct
	public void init() {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.DEBUG);
		this.theme = "bootstrap";
                int error=0;
		try {
                        error=1;
			this.businessDelegate = new BusinessDelegate();
                        error=2;
			this.propertiesFacade = new PropertiesFacade();
                        error=3;
			this.errorHandlerFacade = new ErrorHandlerFacade();
			logger.info("LoginManageBean Cargado correctamente");
		}catch (Exception e) {
			// TODO: handle exception
			 System.out.println("Error #"+error+e.getMessage());
			 generateMessage(e);
			 logger.error("Error #"+error+e.getMessage());
		}

	}

	
	public void setGris(){
		theme = "bootstrap";
		
	}
	
	public void setRed(){
		theme = "blitzer";
		
	}


	public IErrorHandlerFacade getErrorHandlerFacade() {
		return errorHandlerFacade;
	}



	public void setErrorHandlerFacade(IErrorHandlerFacade errorHandlerFacade) {
		this.errorHandlerFacade = errorHandlerFacade;
	}



	public void setPropertiesFacade(IPropertiesFacade propertiesFacade) {
		this.propertiesFacade = propertiesFacade;
	}



	public BusinessDelegate getBusinessDelegate() {
		return businessDelegate;
	}



	public void setBusinessDelegate(BusinessDelegate businessDelegate) {
		this.businessDelegate = businessDelegate;
	}


	public IPropertiesFacade getPropertiesFacade() {
		return propertiesFacade;
	}



	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getAffiliateId() {
		return affiliateId;
	}



	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
	}



	public String getMarket() {
		return market;
	}



	public void setMarket(String market) {
		this.market = market;
	}



	public String getBusinessDomain() {
		return businessDomain;
	}



	public void setBusinessDomain(String businessDomain) {
		this.businessDomain = businessDomain;
	}
	
	
	public String getTheme() {
		return theme;
	}
	
	public void setTheme(String theme) {
		this.theme = theme;
	}
	
	
	
	public String getSystem() {
		return system;
	}



	public void setSystem(String system) {
		this.system = system;
	}



	private AffiliateInformation getAffiliteInformation(){
		AffiliateInformation affiliateInformation = new AffiliateInformation();
		affiliateInformation.setAffiliateID( getAffiliateId() );
		affiliateInformation.setMarket( getMarket() );
		affiliateInformation.setBusinessDomain( getBusinessDomain() );
		return affiliateInformation;
	}
	
	private LoginBean getLoginBean(){
		LoginBean loginBean = new LoginBean();
		loginBean.setUserName( getUserName() );
		loginBean.setPassword( getPassword() );
		loginBean.setAffiliateInformation( getAffiliteInformation() );
		return loginBean;
	}
	
	public void clean(ActionEvent event){
		setUserName("");
		setPassword("");
		setAffiliateId("");
		setMarket("");
		setBusinessDomain("");
		refresh(null);
	}
	
	public void refresh(ActionEvent actionEvent) {
		FacesContext context = FacesContext.getCurrentInstance();
		Application application = context.getApplication();
		ViewHandler viewHandler = application.getViewHandler();
		UIViewRoot viewRoot = viewHandler.createView(context, context
				.getViewRoot().getViewId());
		context.setViewRoot(viewRoot);
		context.renderResponse(); // Optional
	}
	
	public void login(ActionEvent event){
		ResponseLoginBean responseLoginBean = null;
		
		try {
			responseLoginBean = getBusinessDelegate().login( getLoginBean() );
			System.out.println(responseLoginBean.isAnswer());	
			if( responseLoginBean.isAnswer() ){

				FacesContext context = FacesContext.getCurrentInstance();
				HttpSession session = (HttpSession)context.getExternalContext().getSession(true);
				
				session.setAttribute("userId", getLoginBean().getUserName() );
				session.setAttribute("AFFILIATE_ID" , getAffiliteInformation().getAffiliateID() );
				session.setAttribute("AFFILIATE_MARKET" , getAffiliteInformation().getMarket() );
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("username", getLoginBean().getUserName());
				
				context.getExternalContext().redirect( getSystem() );
				
				//HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();  
                //HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();  
                                
                //response.sendRedirect( getSystem() );
               
			}else{
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								getPropertiesFacade().getProperty(Constant.AUTHENTICATION_ERROR),
								responseLoginBean.getResponseFailure().getErrorDescription()));

			}
		} catch (BusinessException e) {
			generateMessage(e);
		} catch (DAOException e) {
			generateMessage(e);
		} catch (AdapterException e) {
			generateMessage(e);
		} catch (ExternalSystemNotAvailable e) {
			generateMessage(e);
		} catch (DataException e) {
			generateMessage(e);
		} catch (Exception e) {
			generateMessage(e);
		}
	}
	
	public void logout( ActionEvent event){
		  FacesContext context = FacesContext.getCurrentInstance();
		  HttpSession session = (HttpSession)context.getExternalContext().getSession(false);
		  session.invalidate();
		  try {
			context.getExternalContext().redirect( "login.xhtml" );
		} catch (IOException e) {
			generateMessage(e);
		}
	}
	
	
	public void generateMessage(Exception e) {
		e.printStackTrace();
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			String systemError = getPropertiesFacade().getProperty(Constant.SYSTEM_ERROR);
			if (e instanceof BusinessException) {

				context.addMessage(
						null,
						new FacesMessage(systemError,
								this.getPropertiesFacade().getProperty(
										Constant.BUSINESS_EXCEPTION)));

			}

			if (e instanceof DAOException) {
                            
                                StringWriter sw = new StringWriter();
                                
                                PrintWriter pw = new PrintWriter(sw, true);
                                e.printStackTrace(pw);                                
				context.addMessage(
						null,
						new FacesMessage(systemError,
								this.getPropertiesFacade().getProperty(
										Constant.DAO_EXCEPTION)));

			}

			if (e instanceof AdapterException) {

				context.addMessage(
						null,
						new FacesMessage(systemError,
								this.getPropertiesFacade().getProperty(
										Constant.ADAPTER_EXCEPTION)));

			}
			
			if (e instanceof ExternalSystemNotAvailable) {

				context.addMessage(
						null,
						new FacesMessage(systemError,
								this.getPropertiesFacade().getProperty(
										Constant.EXTERNAL_SYSTEM_NOT_AVALAIBLE_EXCEPTION)));

			}
			
			if (e instanceof DataException) {

				context.addMessage(
						null,
						new FacesMessage(systemError,
								this.getPropertiesFacade().getProperty(
										Constant.DATA_EXCEPTION)));

			}
			

		} catch ( PropertyException exception ) {			
			context.addMessage(null,new FacesMessage("Mensaje del sistema","Problemas con la autenticacion del sistema"));
			
		}

	}

	
	
}
