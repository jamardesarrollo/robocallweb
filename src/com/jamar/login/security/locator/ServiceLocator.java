package com.jamar.login.security.locator;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceClient;


import com.jamar.exception.MiddlewareException;
import com.jamar.properties.constant.Constant;
import com.jamar.properties.facade.IPropertiesFacade;
import com.jamar.properties.facade.impl.PropertiesFacade;
import com.jamar.security.service.security.SecurityInterface;
import com.jamar.security.service.security.SecurityService;


public class ServiceLocator {

	private static SecurityService securityService; 
	
	
	public static SecurityInterface getSecurityInterface()throws MiddlewareException {
		SecurityInterface securityInterface = null;
		WebServiceClient clienteWS = SecurityService.class.getAnnotation(WebServiceClient.class);
		URL endPoint;
		try{
			IPropertiesFacade propertiesFacade = new PropertiesFacade();
			endPoint = new URL( propertiesFacade.getProperty( Constant.SECURITY_SERVICE_ENDPOINT ) );
			QName serviceName = new QName(clienteWS.targetNamespace(),clienteWS.name());
			securityService = new SecurityService(endPoint,serviceName);
			securityInterface = securityService.getSecurityPort();			
		}catch(MalformedURLException e){
			throw new MiddlewareException(e.getMessage(),e.getCause());
		}catch(Exception e){
			throw new MiddlewareException(e.getMessage(),e.getCause());
		}
		return securityInterface;
	}
}
