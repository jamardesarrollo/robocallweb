package com.jamar.login.security.delegate;


import com.jamar.beans.security.LoginBean;
import com.jamar.beans.security.ResponseLoginBean;
import com.jamar.exception.AdapterException;
import com.jamar.exception.BusinessException;
import com.jamar.exception.DAOException;
import com.jamar.exception.DataException;
import com.jamar.exception.ExternalSystemNotAvailable;
import com.jamar.exception.MiddlewareException;
import com.jamar.login.security.locator.ServiceLocator;
import com.jamar.security.service.security.BusinessFault;
import com.jamar.security.service.security.DataNotFoundFault;
import com.jamar.security.service.security.ExternalSystemNotAvailableFault;
import com.jamar.security.service.security.IllegalUsageFault;
import com.jamar.security.service.security.InternalSystemFault;
import com.jamar.security.service.security.MandatoryDataMissingFault;
import com.jamar.security.service.security.ReferenceDataMissingFault;
import com.jamar.security.service.security.SecurityInterface;
import com.jamar.wsadapter.security.SecurityAdapter;

public class BusinessDelegate {
	private SecurityAdapter securityAdapter;
	private SecurityInterface securityInterface;

	public BusinessDelegate() throws MiddlewareException {
		this.securityAdapter = new SecurityAdapter();
		this.securityInterface = ServiceLocator.getSecurityInterface();
	}

	public SecurityAdapter getSecurityAdapter() {
		return securityAdapter;
	}

	public void setSecurityAdapter(SecurityAdapter securityAdapter) {
		this.securityAdapter = securityAdapter;
	}

	public SecurityInterface getSecurityInterface() {
		return securityInterface;
	}

	public void setSecurityInterface(SecurityInterface securityInterface) {
		this.securityInterface = securityInterface;
	}

	public ResponseLoginBean login(LoginBean loginBean)
			throws BusinessException, DAOException, AdapterException,
			ExternalSystemNotAvailable, DataException, Exception {

		try {

			return getSecurityAdapter().loginResponseXMLToBean(
					getSecurityInterface().login(
							getSecurityAdapter().loginRequestBeanToXML(
									loginBean)));

		} catch (AdapterException e) {
			/*
			 * Problemas en la transformacn de los datos que enva el
			 * webservice para ser convertidos a Java. Si los datos no encajan
			 * de un lado a otro, se genera una excepcin en el Adapter, ya que
			 * no se puede generar la traduccin de XML a Bean(java) o de Bean a
			 * XML.
			 */

			throw e;

		} catch (BusinessFault e) {
			/*
			 * Si no se cumple una regla de negocio, se generar una excepcin
			 * en el Business.
			 */
			throw new BusinessException(e.getMessage(), e.getCause());

		} catch (DataNotFoundFault e) {
			/*
			 * Errores asociados con la base de datos, como errores SQL,
			 * generaran excepciones en el DAO.
			 */
			throw new DAOException(e.getMessage(), e.getCause());

		} catch (ExternalSystemNotAvailableFault e) {
			/* Error de conexin con base de datos o WebServices */
			throw new ExternalSystemNotAvailable(e.getMessage(), e.getCause());

		} catch (IllegalUsageFault e) {
			/*
			 * Errores en los tipos de datos String los cuales se manejan como
			 * numricos. Ejemplo, un campo identificacin el cual solo maneja
			 * nmeros pero es declarado como String dentro de la BD
			 */
			throw new DataException(e.getMessage(), e.getCause());

		} catch (InternalSystemFault e) {
			/*
			 * Errores fatales a nivel del servicio. Como encolamiento de
			 * peticiones.
			 */
			throw new MiddlewareException(e.getMessage(), e.getCause());

		} catch (MandatoryDataMissingFault e) {
			/* Los campos requeridos por el servicio no se envan completos */
			throw new DataException(e.getMessage(), e.getCause());

		} catch (ReferenceDataMissingFault e) {
			/*
			 * El dato que se enva no existe en la base de datos. Tablas de
			 * parmetros o catlogos.
			 */
			throw new DataException(e.getMessage(), e.getCause());

		} catch (Exception e) {
			/* Dems excepciones que surjan. */
			throw e;
		}
	}
}
